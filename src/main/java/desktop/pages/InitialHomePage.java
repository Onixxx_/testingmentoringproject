package desktop.pages;

import abstractClasses.page.AbstractPage;
import desktop.fragments.Header;

public class InitialHomePage extends AbstractPage {

    private final String URL = "https://www.bookdepository.com/";

    public InitialHomePage() {
        super();
        setPageUrl(URL);
    }

    public Header getHeader() {
        return new Header();
    }
}
