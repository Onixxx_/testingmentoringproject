package dto;

public class Card {

    private final String cardNumber;
    private final String expiryYear;
    private final String expiryMonth;
    private final String cvv;


    public Card(String cardNumber, String expiryYear, String expiryMonth, String cvv) {
        this.cardNumber = cardNumber;
        this.expiryYear = expiryYear;
        this.expiryMonth = expiryMonth;
        this.cvv = cvv;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public String getCvv() {
        return cvv;
    }
}
