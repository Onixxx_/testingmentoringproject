package utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import static driver.SingletonDriver.getDriver;

public class JavaScriptExecutorActions {

    private JavaScriptExecutorActions() {
    }

    public static void clickButtonUsingJS(WebElement button) {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();
        jse.executeScript("arguments[0].click();", button);
    }

    public static void scrollToElement(WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
