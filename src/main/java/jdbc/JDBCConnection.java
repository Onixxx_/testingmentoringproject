package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

    private Connection dbConnection;

    public void setUpDriverDb() {
        String url = "jdbc:mysql://localhost:3306/testjdbc?useUnicode=true&serverTimezone=UTC";
        String username = "user1";
        String password = "user1!";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Connection to database..");

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            dbConnection = DriverManager.getConnection(url, username, password);
            System.out.println("Database connected!");
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("Can not connect to database:(");
        }
    }


    public Connection getDbConnection() {
        return dbConnection;
    }
}
