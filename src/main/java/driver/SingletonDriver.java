package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;


import static constants.Constants.IMPLICITLY_WAIT_TIMEOUT;
import static driver.CapabilitiesHelper.getChromeOptions;
import static driver.CapabilitiesHelper.getFirefoxOptions;

public class SingletonDriver {

    private static WebDriver instance;

    private SingletonDriver() {
    }

    public static WebDriver getDriver() {
        if (instance == null) {
            instance = createDriver();
            instance.manage().window().maximize();
            instance.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT, TimeUnit.SECONDS);
        }
        return instance;
    }

    public static void openPageByUrl(String url) {
        instance.get(url);
    }

    public static WebDriver createDriver() {
        String browser = System.getProperty("browser");

        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                return new ChromeDriver(getChromeOptions());
            case "firefox":
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                return new FirefoxDriver(getFirefoxOptions());
            default:
                throw new IllegalStateException("This driver is not supported");
        }
    }
}
