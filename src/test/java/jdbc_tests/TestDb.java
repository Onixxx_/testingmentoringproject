package jdbc_tests;

import jdbc.JDBCConnection;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;

public class TestDb {

    private JDBCConnection connection;

    @Test
    public void simpleTestDb() throws SQLException {
        connection = new JDBCConnection();
        connection.setUpDriverDb();
        Statement stm = connection.getDbConnection().createStatement();
        ResultSet rs = stm.executeQuery("SELECT name, count FROM products where shopId = 3");
        int size = 0;
        while (rs.next())
        {
            size++;
        }
        assertEquals(3, size);
    }

    @Test
    public void complicatedTestDb() throws SQLException {
        connection = new JDBCConnection();
        connection.setUpDriverDb();
        Statement stm = connection.getDbConnection().createStatement();
        ResultSet rs = stm.executeQuery("select count(name) as productsCount " +
                "from products join shops on products.shopId = shops.id " +
                "where nameOfShop = 'Sweet world' " +
                "group by nameOfShop");
        rs.next();
        int count = rs.getInt("productsCount");
        assertEquals(2, count);
    }
}
